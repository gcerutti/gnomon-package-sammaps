#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "Gnomon python plugins to analyze sequences of SAM images"
readme = open('README.md').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_sammaps',
    version="1.0.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Guillaume Cerutti",
    author_email="guillaume.cerutti@ens-lyon.fr",
    url='',
    license='LGPL-3.0-or-later',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    package_data={
        "": [
            "*.png",
            "*/*.png",
            "*/*/*.png",
            "*.json",
            "*/*.json",
            "*/*/*.json"
        ]
    },

    entry_points={
        'meshFilter': [
            'meshMeristemDelineation = gnomon_package_sammaps.algorithm.meshFilter.meshMeristemDelineation',
        ],
        'pointCloudQuantification': [
            'clv3qDIISamAlignment = gnomon_package_sammaps.algorithm.pointCloudQuantification.clv3qDIISamAlignment',
            'clv3SamCentering = gnomon_package_sammaps.algorithm.pointCloudQuantification.clv3SamCentering',
            'curvatureSamAlignment = gnomon_package_sammaps.algorithm.pointCloudQuantification.curvatureSamAlignment',
            'nucleiRigidRegistration = gnomon_package_sammaps.algorithm.pointCloudQuantification.nucleiRigidRegistration',
            'spiralPhyllotaxisPrimordiaDetection = gnomon_package_sammaps.algorithm.pointCloudQuantification.spiralPhyllotaxisPrimordiaDetection',
        ],
        'dataFrameMplVisualization': [
            'matplolibMapPlot = gnomon_package_sammaps.visualization.dataFrameMplVisualization.matplolibMapPlot',
        ],
    },
    keywords='',
    
    test_suite='nose.collector',
    )

setup(**setup_kwds)
