import os
import logging
from copy import deepcopy

import pandas as pd
import numpy as np

from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from PIL import Image

from visu_core.matplotlib.colormap import plain_colormap

from maps_2d.signal_map import SignalMap, find_center_and_extent
from maps_2d.signal_map_visualization import signal_2d_map_plot, signal_map_contours_plot, confidence_and_frame_plot

from dtkcore import d_bool, d_int, d_inliststring, d_real, d_range_real, array_real_2

import gnomon.visualization
from gnomon.visualization import gnomonAbstractDataFrameMplVisualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import dataFrameInput
from gnomon.utils.matplotlib_tools import gnomon_figure


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Map plot")
@dataFrameInput('df', data_plugin="gnomonDataFrameDataPandas")
class matplotlibMapPlot(gnomonAbstractDataFrameMplVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['position_variable'] = d_inliststring("Position Variable", "", [""], "Columns to use as spatial reference")
        self._parameters['map_variable'] = d_inliststring("Map Variable", "", [""], "Column to actually plot as a 2D map")
        self._parameters['filter_variable'] = d_inliststring("Filter Variable", "", [""], "Column to use as a filter on data points")
        self._parameters["filter_range"] = d_range_real("Filter range", array_real_2([0., 1.]), 0., 1., "Value range for cell filtering")

        self._parameters['all_times'] = d_bool("Merge all times", False, "Whether to merge all time points or consider them separately")

        self._parameters['resolution'] = d_real("Resolution", 1, 0, 10, 2, "Size of the XY elements of the 2D map")
        self._parameters['radius'] = d_real("Radius", 7.5, 0, 100, 1, "Radius used for the 2D map generation")
        self._parameters['density_k'] = d_real("Sharpness", 0.55, 0, 10, 2, "Sharpness used for the 2D map generation")

        self._parameters['colormap'] = gnomon.visualization.ParameterColorMap('colormap', 'viridis', 'Colormap to apply on the map')
        self._parameters['value_range'] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters['contour_range'] = d_range_real("Contour range", array_real_2([0., 1.]), 0., 1., "Value range for contour plotting")
        self._parameters['n_contours'] = d_int("Number of contours", 10, 0, 51, "Number of contour lines to display")

        self.connectParameter("map_variable")
        self.connectParameter("filter_variable")

        self._parameter_groups = {}
        for parameter_name in ['filter_variable', 'filter_range']:
            self._parameter_groups[parameter_name] = 'filter'
        for parameter_name in ['resolution', 'radius', 'density_k']:
            self._parameter_groups[parameter_name] = 'map_generation'
        for parameter_name in ['contour_range', 'n_contours']:
            self._parameter_groups[parameter_name] = 'contours'

        self.df = {}
        self.signal_map = None

        self._current_value = {}
        self.current_time = None
        self.figure = None

    def __del__(self):
        if self.figure is not None:
            self.figure.clf()

    def refreshParameters(self):
        if len(self.df) > 0:
            df = list(self.df.values())[0]
            
            variables = [v for v in df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(df[v]).ndim == 1]
            self._parameters['map_variable'].setValues(variables)
            if len(variables)>0:
                self._parameters['map_variable'].setValue(variables[0])
                self._update_value_range()
    
            self._parameters['filter_variable'].setValues([""] + variables)
            self._parameters['filter_variable'].setValue("")
    
            variable_prefixes = np.unique([v[:-2] for v in variables])
            position_variables = [v for v in variable_prefixes if all([f"{v}_{dim}" in variables for dim in "xyz"])]
            self._parameters['position_variable'].setValues(position_variables)
            if len(position_variables)>0:
                self._parameters['position_variable'].setValue(position_variables[0])

        self._current_value = {
            p: None for p in self._parameters.keys()
        }

    def onParameterChanged(self, parameter_name):
        if parameter_name in ['map_variable', 'filter_variable']:
            self._update_value_range()

    def clear(self):
        if self.figure is not None:
            self.figure.clf()
            self.figure.canvas.draw()

    def setVisible(self, visible):
        pass

    def _update_value_range(self):
        if self.current_time in self.df:
            df = self.df[self.current_time]
            map_values = df[self['map_variable']].values

            min_p = float(np.around(np.nanmin(map_values), decimals=3))
            max_p = float(np.around(np.nanmax(map_values), decimals=3))

            for range_param in ['value_range', 'contour_range']:
                self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
                self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
                self._parameters[range_param].setValueMin(min_p)
                self._parameters[range_param].setValueMax(max_p)

            if self['filter_variable'] in df.columns:
                filter_values = df[self['filter_variable']].values

                min_p = float(np.around(np.nanmin(filter_values), decimals=3))
                max_p = float(np.around(np.nanmax(filter_values), decimals=3))

                for range_param in ['filter_range']:
                    self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
                    self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
                    self._parameters[range_param].setValueMin(min_p)
                    self._parameters[range_param].setValueMax(max_p)

    def update(self):
        self.figure = gnomon_figure(self.figureNumber())
        self.current_time = self.view().currentTime()
        if self.current_time in self.df:
            if self['all_times']:
                time_dfs = []
                for time in self.df:
                    time_df = self.df[time]
                    time_df['time'] = time
                    time_dfs.append(time_df)
                df = pd.concat(time_dfs)
            else:
                df = self.df[self.current_time]

            self.figure.clf()
            map_df = deepcopy(df)

            filter_changed = self._current_value['filter_variable'] != self['filter_variable']
            filter_changed |= (self._current_value['filter_range'] is None) or any([
                self._current_value['filter_range'][k] != self['filter_range'][k]
                for k in range(2)
            ])
            if self['filter_variable'] in map_df.columns:
                filter_values = map_df[self['filter_variable']].values
                filter = filter_values >= self['filter_range'][0]
                filter &= filter_values <= self['filter_range'][1]
                map_df = map_df[filter]

                self._current_value['filter_variable'] = self['filter_variable']
                self._current_value['filter_range'] = [self['filter_range'][k] for k in range(2)]

                self.signal_map = None

            map_changed = any([
                self._current_value[p] != self[p]
                for p in ['resolution', 'radius', 'density_k', 'position_variable']
            ])
            if map_changed or self.signal_map is None:
                extent, origin = find_center_and_extent(map_df, self['position_variable'])
                self.signal_map = SignalMap(
                    map_df,
                    origin = origin,
                    extent = extent + 2*self['radius'],
                    position_name=self['position_variable'],
                    resolution=self['resolution'],
                    radius=self['radius'],
                    density_k=self['density_k']
                )
                for p in ['resolution', 'radius', 'density_k', 'position_variable']:
                    self._current_value[p] = self[p]

            self.signal_map.compute_signal_map(self['map_variable'])

            col = signal_2d_map_plot(
                self.signal_map,
                self['map_variable'],
                self.figure.gca(),
                colormap=self._parameters['colormap'].name(),
                signal_lut_range=self['value_range']
            )

            if self['n_contours'] > 0:
                signal_map_contours_plot(
                    self.signal_map,
                    self['map_variable'],
                    self.figure.gca(),
                    contour_range=self['contour_range'],
                    signal_lut_range=self['contour_range'],
                    colormap=plain_colormap('w'),
                    alpha=0.5,
                    n_contours=self['n_contours']
                )

            confidence_and_frame_plot(self.signal_map, self.figure.gca())

            self.figure.gca().axis('equal')
            self.figure.gca().set_xlabel(f"{self['position_variable']} X", size=14)
            self.figure.gca().set_ylabel(f"{self['position_variable']} Y", size=14)

            if col is not None:
                main_axis = self.figure.gca()
                cax = inset_axes(self.figure.gca(),width="3%", height="25%", loc='lower right')
                cbar = self.figure.colorbar(col, cax=cax, pad=0.)
                cax.yaxis.set_ticks_position('left')
                cax.set_ylabel(self['map_variable'], size=14)
                #cbar.set_clim(*signal_lut_ranges[signal_name])
                self.figure.sca(main_axis)

        self.render()

    def render(self):
        if self.figure is not None:
            #self.figure.tight_layout()
            self.figure.canvas.draw()

    def imageRendering(self):
        if self.figure is not None:
            w, h = self.figure.canvas.get_width_height()

            canvas_img = np.fromstring(self.figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
            if (canvas_img.shape[0]/3) == 4*h*w:
                canvas_img = canvas_img.reshape(2*h, 2*w, 3)
            elif (canvas_img.shape[0]/3) == (2*h)*(2*w+1):
                canvas_img = canvas_img.reshape(2*h, 2*w+1, 3)
            elif (canvas_img.shape[0]/3) == (2*h+1)*(2*w):
                canvas_img = canvas_img.reshape(2*h+1, 2*w, 3)
            elif (canvas_img.shape[0]/3) == h*w:
                canvas_img = canvas_img.reshape(h, w, 3)
            else:
                canvas_img = np.zeros((600, 600, 3), float)

            canvas_img = np.array(Image.fromarray(canvas_img).resize((600, 600)))
        else:
            canvas_img = np.zeros((600, 600, 3), float)

        self.image = [[[rgb for rgb in c] for c in r] for r in canvas_img]
        return self.image

    def onTimeChanged(self, value):
        if value in self.df:
            time_changed =  value != self.current_time
            self.current_time = value
            if time_changed:
                self.signal_map = None
                self.update()
