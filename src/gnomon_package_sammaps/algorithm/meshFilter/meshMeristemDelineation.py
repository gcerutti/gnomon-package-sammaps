from copy import deepcopy

import numpy as np

from dtkcore import d_real, d_inliststring

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, meshInput, meshOutput


from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces

from sam_atlas.meristem_surface import curvature_meristem_extraction


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@cellImageInput("seg_img", data_plugin="gnomonCellImageDataTissueImage")
@meshOutput('out_surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class meshMeristemDelineation(gnomon.core.gnomonAbstractMeshFilter):
    """
    Isolate the central meristem region in a SAM surface mesh

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['curvature_property'] = d_inliststring('Curvature', 'principal_curvature_min', ['principal_curvature_min', 'principal_curvature_max', 'mean_curvature', 'gaussian_curvature'], "Type of curvature to use")
        self._parameters['curvature_threshold'] = d_real('Threshold', -1e-3, -0.1, 0.1, 3, "Curvature threshold used to discriminate the meristem region")

        self.surface_topomesh = {}
        self.seg_img = {}
        self.out_surface_topomesh = {}

    def run(self):
        self.out_surface_topomesh = {}

        for time in self.surface_topomesh.keys():
            surface_topomesh = deepcopy(self.surface_topomesh[time])

            compute_topomesh_property(surface_topomesh, 'area', 2)
            compute_topomesh_property(surface_topomesh, 'barycenter', 2)
            if not 'principal_curvature_min' in surface_topomesh.wisp_property_names(2):
                compute_topomesh_property(surface_topomesh, 'normal', 2, normal_method='orientation')
                compute_topomesh_vertex_property_from_faces(surface_topomesh, 'normal', neighborhood=3, adjacency_sigma=1.2)
                compute_topomesh_property(surface_topomesh, 'mean_curvature', 2)

            img_center = np.nanmean(surface_topomesh.wisp_property("barycenter", 0).values(), axis=0)
            if time in self.seg_img:
                seg_img = self.seg_img[time]
                img_center = ((np.array(seg_img.shape)-1)*np.array(seg_img.voxelsize)/2)[::-1]

            meristem_topomesh = curvature_meristem_extraction(
                surface_topomesh,
                curvature_property=self["curvature_property"],
                curvature_threshold=self["curvature_threshold"],
                image_center=img_center
            )

            self.out_surface_topomesh[time] = surface_topomesh
