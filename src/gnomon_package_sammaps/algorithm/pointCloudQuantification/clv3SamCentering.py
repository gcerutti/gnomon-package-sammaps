import logging
from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from dtkcore import d_bool, d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, pointCloudOutput, dataFrameOutput
from gnomon.core import gnomonAbstractPointCloudQuantification

from sam_atlas.centering import center_sam_sequence
from maps_2d.epidermal_maps import compute_local_2d_signal, nuclei_density_function


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="SAM CLV3 Centering")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class clv3SamCentering(gnomonAbstractPointCloudQuantification):
    """Compute the centered positions of SAM nuclei using biological signals.

    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['all_times'] = d_bool("All Times", True, "Whether to compute the same center for all time points (using registered positions)")
        self._parameters['alignment_times'] = d_inliststringlist("Centering Times", [], [], "Which time points to use to compute the centering transformation")

    def refreshParameters(self):
        print("refreshParameters", self.df)

        if len(self.df)>0:
            sorted_times = list(np.sort([str(t) for t in self.df.keys()]))
            print(sorted_times)

            if len(sorted_times) == 1:
                if 'alignment_times' in self._parameters.keys():
                    del self._parameters['alignment_times']
            else:
                if 'alignment_times' not in self._parameters.keys():
                    self._parameters['alignment_times'] = d_inliststringlist("Centering Times", [], [], "Which time points to use to compute the centering transformation")
                    times = sorted_times
                else:
                    times = self['alignment_times'] if len(self['alignment_times'])>0 else sorted_times
                self._parameters['alignment_times'].setValues(sorted_times)
                self._parameters['alignment_times'].setValue(times)

    def run(self):
        self.data_df = {}

        self.out_df = {time: deepcopy(df) for time, df in self.df.items()}

        if self['all_times']:
            if 'alignment_times' in self._parameters.keys():
                alignment_times = [float(t) for t in self['alignment_times']]
            else:
                alignment_times = [float(t) for t in self.df]

            sequence_data = {f"sam_t{str(int(np.round(time))).zfill(2)}": out_df for time, out_df in self.out_df.items()}
            centering_filenames = [f"sam_t{str(int(np.round(time))).zfill(2)}" for time in alignment_times]
            logging.getLogger().setLevel(logging.INFO)
            center_sam_sequence(
                sequence_data,
                sequence_rigid_transforms={},
                alignment_filenames=centering_filenames,
                r_max=120,
                cell_radius=5.,
                density_k=0.25,
                microscope_orientation=-1,
            )
        else:
            sequence_data = {}
            for time, out_df in self.out_df.items():
                time_filename = f"sam_t{str(int(np.round(time))).zfill(2)}"
                time_sequence_data = {time_filename: out_df}
                centering_filenames = [time_filename]
                logging.getLogger().setLevel(logging.INFO)
                center_sam_sequence(
                    time_sequence_data,
                    sequence_rigid_transforms={},
                    alignment_filenames=centering_filenames,
                    r_max=120,
                    cell_radius=5.,
                    density_k=0.25,
                    microscope_orientation=-1,
                )
                sequence_data.update(time_sequence_data)

        for time in self.df.keys():
            out_df = sequence_data[f"sam_t{str(int(np.round(time))).zfill(2)}"]

            centered_positions = out_df[[f'centered_{dim}' for dim in 'xyz']].values
            out_df['radial_distance'] = np.linalg.norm([centered_positions[:, 0], centered_positions[:, 1]], axis=0)

            self.out_df[time] = out_df

            self.data_df[time] = deepcopy(out_df)
