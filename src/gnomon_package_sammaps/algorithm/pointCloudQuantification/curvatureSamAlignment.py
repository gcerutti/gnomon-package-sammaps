import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_real, d_inliststring

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, pointCloudOutput, dataFrameOutput
from gnomon.core import gnomonAbstractPointCloudQuantification

from sam_atlas.alignment import load_aligned_sam_database, default_database_dirname
from sam_atlas.alignment import centered_meristem_curvature_map_alignment


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Curvature SAM Alignment")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class curvatureSamAlignment(gnomonAbstractPointCloudQuantification):
    """Compute the aligned positions of SAM nuclei using surface curvature maps

    """

    def __init__(self):
        super().__init__()

        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['microscope_orientation'] = d_inliststring("Microscope Orientation", "upright", ["upright", "inverted"], "The orientation of the microscope used to acquire images")
        self._parameters['pz_radius'] = d_real("PZ Radius", 60, 0, 120, 1, "The radial distance up to which to compare curvature maps")
        self._parameters['curvature_type'] = d_inliststring("Curvature type", "mean_curvature", ["mean_curvature", "gaussian_curvature"], "The type of curvature used for comparison")

    def run(self):
        self.data_df = {}

        self.out_df = {time: deepcopy(df) for time, df in self.df.items()}

        sequence_data = {f"sam_t{str(int(np.round(time))).zfill(2)}": out_df for time, out_df in self.out_df.items()}

        database_dirname = default_database_dirname
        aligned_sequence_database = load_aligned_sam_database(database_dirname)

        centered_meristem_curvature_map_alignment(
            sequence_data, aligned_sequence_database,
            curvature_type=self['curvature_type'],
            map_resolution=1, pz_radius=self['pz_radius']
        )

        for time in self.df.keys():
            out_df = sequence_data[f"sam_t{str(int(np.round(time))).zfill(2)}"]
            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)

