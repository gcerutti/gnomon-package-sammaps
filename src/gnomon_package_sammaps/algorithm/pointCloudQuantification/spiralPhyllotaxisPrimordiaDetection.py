import logging
from copy import deepcopy

from dtkcore import d_bool, d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.core import gnomonAbstractPointCloudQuantification
from gnomon.utils.decorators import pointCloudInput, pointCloudOutput, dataFrameOutput

import numpy as np
import pandas as pd

from maps_2d.signal_map import SignalMap
from maps_2d.signal_map_analysis import compute_signal_map_landscape, signal_map_landscape_analysis
from maps_2d.epidermal_maps import compute_local_2d_signal

from sam_atlas.primordia_detection import label_primordia_extrema


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Primordia detection")
@pointCloudInput("nuclei_df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput("primordia_df", data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('primordia_data_df', data_plugin="gnomonDataFrameDataPandas")
class spiralPhyllotaxisPrimordiaDetection(gnomonAbstractPointCloudQuantification):
    """Detect primordia assuming a regular spiral phyllotaxis

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'r_max': d_real("Max radius", 90, 0, 200, 1, "Maximal radius up to which to search for primordia"),
            'cell_radius': d_real("Cell radius", 5., 0, 100, 1, "Radius used for the 2D map generation"),
            'density_k': d_real("Sharpness", 0.25, 0, 10, 2, "Sharpness used for the 2D map generation"),
        }
        
        self.nuclei_df = {}
        self.primordia_df = {}
        self.primordia_data_df = {}

    def run(self):
        self.primordia_df = {}

        for time in self.nuclei_df.keys():
            nuclei_df = self.nuclei_df[time]
            # #}
            # implement the run method

            nuclei_df = nuclei_df.query("layer==1")
            X = nuclei_df['aligned_x'].values
            Y = nuclei_df['aligned_y'].values

            if 'clv3_radius' in nuclei_df.columns:
                clv3_radius = nuclei_df['clv3_radius'].mean()
            else:
                clv3_radius = 28.

            if not 'Normalized_qDII' in nuclei_df.columns:
                signal_values = nuclei_df['qDII'].values
                nuclei_df["Normalized_qDII"] = 0.5 + 0.2*(signal_values - signal_values.mean())/(signal_values.std())

            logging.info(f"--> Computing qDII landscape T={time}")

            file_map = SignalMap(
                nuclei_df, extent=self['r_max'], position_name='aligned',
                resolution=0.5, polar=False, radius=self['cell_radius'],
                density_k=self['density_k']
            )
            file_map.compute_signal_map('Normalized_qDII')
    
            signal_name = "Normalized_qDII"
    
            compute_signal_map_landscape(file_map, [signal_name])

            logging.info(f"--> Detecting landscape extremal points T={time}")
            extrema_data = signal_map_landscape_analysis(file_map, signal_name, threshold=0.03)
    
            extrema_data['score'] = 1
            extrema_data['score'] *= [(1-s) if t=='minimum' else 0.2+s if t=='saddle' else s for s,t in extrema_data[[signal_name, 'extremum_type']].values]
            # extrema_data['score'][extrema_data['extremum_type']=='minimum'] *= 1. - extrema_data[extrema_data['extremum_type']=='minimum'][signal_name]
            # extrema_data['score'][extrema_data['extremum_type']=='maximum'] *= extrema_data[extrema_data['extremum_type']=='maximum'][signal_name]
            # extrema_data['score'][extrema_data['extremum_type']=='saddle'] *= 0.2 + extrema_data[extrema_data['extremum_type']=='saddle'][signal_name]
            extrema_data['score'] *= np.minimum(1, 0.5+extrema_data['extremality'])
            # extrema_data['score'][extrema_data['extremum_type']!='saddle'] *= 1 - 1./extrema_data[extrema_data['extremum_type']!='saddle']['area']
            extrema_data['score'] *= [1 - 1./a if t != 'saddle' else 1 for a,t in extrema_data[['area','extremum_type']].values]
            extrema_data['score'] *= (extrema_data['radial_distance']/clv3_radius)>0.5
            extrema_data = extrema_data[extrema_data['score']>0]

            logging.info(f"--> Labelling extremal points by primordia T={time}")
    
            opening_angle = 75.
            primordia_extrema_data = label_primordia_extrema(extrema_data, signal_name, clv3_radius, opening_angle=opening_angle)

            primordia_extrema_data['clv3_radius'] = [clv3_radius for i in range(len(primordia_extrema_data))]
            # primordia_extrema_data['filename'] = [filename for i in range(len(primordia_extrema_data))]
            # primordia_extrema_data['experiment'] = [nuclei_df['experiment'].values[0] for i in range(len(primordia_extrema_data))]
            # primordia_extrema_data['sam_id'] = [nuclei_df['sam_id'].values[0] for i in range(len(primordia_extrema_data))]
            # primordia_extrema_data['hour_time'] = [nuclei_df['hour_time'].values[0] for i in range(len(primordia_extrema_data))]
            # primordia_extrema_data['growth_condition'] = ['LD' if 'LD' in filename else 'SD' for i in range(len(primordia_extrema_data))]
    
            for field in ['aligned_z', 'CLV3', 'Normalized_CLV3', 'DIIV', 'Normalized_DIIV', 'qDII', 'Auxin', 'DR5', 'PIN1']:
                if field in nuclei_df.columns:
                    primordia_extrema_data[field] = compute_local_2d_signal(
                        np.transpose([X, Y]),
                        np.transpose([
                            primordia_extrema_data['aligned_x'],
                            primordia_extrema_data['aligned_y']
                        ]),
                        nuclei_df[field].values
                    )

            self.primordia_df[time] = primordia_extrema_data

        for time in self.primordia_df.keys():
            self.primordia_data_df[time] = deepcopy(self.primordia_df[time])