import logging
from time import time as current_time
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_int, d_real, d_inliststring

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import pointCloudInput
from gnomon.utils.decorators import pointCloudOutput

from gnomon.core import gnomonAbstractPointCloudQuantification

from timagetk.algorithms.trsf import inv_trsf, apply_trsf
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.pointmatching import apply_trsf_to_points


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Rigid Registration")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class nucleiRigidRegistration(gnomonAbstractPointCloudQuantification):
    """
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['registration_channel'] = d_inliststring("Registration Channel", "", [""], "Signal channel on which to perform the temporal registration")
        self._parameters['pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img) == 1:
                if 'registration_channel'  in self._parameters.keys():
                    del self._parameters['registration_channel']
            else:
                if 'registration_channel' not in self._parameters.keys():
                    self._parameters['registration_channel'] = d_inliststring("Registration Channel", "", [""], "Signal channel on which to perform the temporal registration")
                    channel_name = img.channel_names[0]
                else:
                    channel_name = self['registration_channel']
                self._parameters['registration_channel'].setValues(img.channel_names)
                self._parameters['registration_channel'].setValue(channel_name)

    def run(self):
        self.data_df = {}
        self.out_df = {}

        sorted_times = np.sort(list(self.df.keys()))
        assert all([t in self.img.keys() for t in sorted_times])

        reference_images = {}
        for time in sorted_times:
            df = self.df[time]
            out_df = deepcopy(df)
            self.out_df[time] = out_df

            img = self.img[time]
            if 'channel' in self._parameters.keys():
                reference_img = img.get_channel(self['channel'])
            else:
                reference_img = img.get_channel(img.channel_names[0])
            reference_images[time] = reference_img

        transformed_images = {}
        transformed_images[sorted_times[0]] = reference_images[sorted_times[0]]
        for k, dim in enumerate("xyz"):
            self.out_df[sorted_times[0]][f'registered_{dim}'] = self.out_df[sorted_times[0]][f'center_{dim}'].values

        rigid_transformations = {}
        for i_t, (previous_time, next_time) in enumerate(zip(sorted_times[:-1], sorted_times[1:])):
            previous_df = self.out_df[previous_time]
            next_df = self.out_df[next_time]

            reference_img = transformed_images[previous_time]
            floating_img = reference_images[next_time]

            start_time = current_time()
            logging.info(f"  --> Computing rigid transformation t={previous_time} <-- t={next_time}")

            rigid_trsf = blockmatching(
                floating_image=floating_img,
                reference_image=reference_img,
                method='rigid',
                pyramid_lowest_level=self['pyramid_lowest'],
                pyramid_highest_level=self['pyramid_highest'],
            )
            logging.info(f"  <-- Computing rigid transformation t={previous_time} <-- t={next_time} [{start_time-current_time()}s]")
            rigid_transformations[(previous_time, next_time)] = rigid_trsf.get_array()

            transformed_images[next_time] = apply_trsf(floating_img, rigid_trsf, template_img=reference_img)

            invert_rigid_trsf = inv_trsf(rigid_trsf)
            rigid_transformations[(next_time, previous_time)] = invert_rigid_trsf.get_array()

            next_points = next_df[[f'center_{dim}' for dim in 'xyz']].values
            registered_points = apply_trsf_to_points(next_points, invert_rigid_trsf)
            for k, dim in enumerate("xyz"):
                next_df[f'registered_{dim}'] = registered_points[:, k]

        for time in self.df.keys():
            self.data_df[time] = deepcopy(self.out_df[time])
