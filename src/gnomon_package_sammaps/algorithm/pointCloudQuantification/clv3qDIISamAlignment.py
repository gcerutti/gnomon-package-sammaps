import logging
from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from dtkcore import d_inliststring, d_inliststringlist, d_int

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, pointCloudOutput, dataFrameOutput
from gnomon.core import gnomonAbstractPointCloudQuantification

from sam_atlas.centering import center_sam_sequence
from sam_atlas.alignment import centered_meristem_qDII_alignment


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="qDII-CLV3 SAM Alignment")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class clv3qDIISamAlignment(gnomonAbstractPointCloudQuantification):
    """Compute the aligned positions of SAM nuclei using biological signals.

    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['sam_orientation'] = d_inliststring("SAM Orientation", "clockwise", ["clockwise", "counterclockwise"], "The orientation of the SAM phyllotactic spiral, from younger to older organs")
        self._parameters['microscope_orientation'] = d_inliststring("Microscope Orientation", "upright", ["upright", "inverted"], "The orientation of the microscope used to acquire images")
        self._parameters['alignment_times'] = d_inliststringlist("Alignment Times", [], [], "Which time points to use to compute the alignment transformation")

    def refreshParameters(self):
        print("refreshParameters", self.df)

        if len(self.df)>0:
            sorted_times = list(np.sort([str(t) for t in self.df.keys()]))
            print(sorted_times)

            if len(sorted_times) == 1:
                if 'alignment_times' in self._parameters.keys():
                    del self._parameters['alignment_times']
            else:
                if 'alignment_times' not in self._parameters.keys():
                    self._parameters['alignment_times'] = d_inliststringlist("Alignment Times", [], [], "Which time points to use to compute the alignment transformation")
                    times = sorted_times
                else:
                    times = self['alignment_times'] if len(self['alignment_times'])>0 else sorted_times
                self._parameters['alignment_times'].setValues(sorted_times)
                self._parameters['alignment_times'].setValue(times)

    def run(self):
        self.data_df = {}

        self.out_df = {time: deepcopy(df) for time, df in self.df.items()}

        if 'alignment_times' in self._parameters.keys():
            alignment_times = [float(t) for t in self['alignment_times']]
        else:
            alignment_times = [float(t) for t in self.df]

        sequence_data = {f"sam_t{str(int(np.round(time))).zfill(2)}": out_df for time, out_df in self.out_df.items()}
        alignment_filenames = [f"sam_t{str(int(np.round(time))).zfill(2)}" for time in alignment_times]
        logging.getLogger().setLevel(logging.INFO)

        microscope_orientation = -1 if (self['microscope_orientation'] == 'inverted') else 1

        center_sam_sequence(
            sequence_data,
            sequence_rigid_transforms={},
            alignment_filenames=alignment_filenames,
            r_max=120.,
            cell_radius=5.,
            density_k=0.25,
            microscope_orientation=microscope_orientation,
        )

        centered_meristem_qDII_alignment(
            sequence_data,
            meristem_orientation=self["sam_orientation"],
            r_max=120.,
            clv3_radius=28.,
            pz_bounds=(0.9, 1.6),
            alignment_filenames=alignment_filenames
        )

        for time in self.df.keys():
            out_df = sequence_data[f"sam_t{str(int(np.round(time))).zfill(2)}"]
            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)
