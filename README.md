# Gnomon plugin package : `SAM Maps`

This package contains Python plugins for the [Gnomon computational platform](https://gnomon.gitlabpages.inria.fr/gnomon/) to analyze time-lapse sequences of SAM images.