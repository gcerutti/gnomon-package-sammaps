import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellComplexFromCellImage(unittest.TestCase):
    """Tests the gnomonCellComplexFromCellImage class.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("cellImageReader")
        load_plugin_group("cellComplexFromCellImage")

    def setUp(self):
        self.filename = "test/resources/E37_SAM7_t00-P3_PI_seg.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.cellImage = self.reader.cellImage()

        self.draco_stem = gnomon.core.cellComplexFromCellImage_pluginFactory().create("cellReconstructionDracoStem")

    def tearDown(self):
        self.reader.this.disown()
        self.draco_stem.this.disown()

    def test_gnomonCellComplexFromCellImage_2d(self):
        self.draco_stem.setInput(self.cellImage)
        self.draco_stem.refreshParameters()
        self.draco_stem.setParameter("dimension", 3)
        self.draco_stem.run()
        assert self.draco_stem.output() is not None
        cellcomplex = self.draco_stem.output()[0]
        assert cellcomplex.elementCount(3) > 0

# test_gnomonCellComplexFromCellImage.py ends here.
